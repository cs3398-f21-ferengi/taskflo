# TaskFlo
> A web program to help with task management and planning.

- Created by Chris Metzler, Fernando Perez Rosas, Jose Tabares, Patrick Stanley, and Rain Lawson
- We are working together to make a web application to help people plan and track their tasks.
- This project was designed to help anyone and everyone who wants help with planning.
- We hope to improve ourselves as coders and software engineers while potentially helping the busy people of the world.

## Table of Contents
* [General Info](#general-information)
* [Technologies Used](#technologies-used)
* [Features](#features)
* [Screenshots](#screenshots)
* [Setup](#setup)
* [Usage](#usage)
* [Project Status](#project-status)
* [Room for Improvement](#room-for-improvement)
* [Acknowledgements](#acknowledgements)
* [Contact](#contact)
<!-- * [License](#license) -->


## General Information
![Image](https://bitbucket.org/cs3398-f21-ferengi/taskflo/downloads/logo.PNG)


## Technologies Used
- Java SE 16.0.2
Springboot
AngularJS
Postgres
Figma

## Features
- TMF: Custom Task Management Tracker to add GUI buttons so the user can add, delete and complete tasks. Story submitted by Patrick Stanley

- TaskTemperature: Color coordinated tasks grouped by type submitted by the user. Story submitted by Jose Tabares

- CalenderFlo: Monthly calender layout for task scheduling. Story submitted by Rain Lawson


## Screenshots
- Server Details
![Image](https://bitbucket.org/cs3398-f21-ferengi/taskflo/downloads/SERVER_DETAILS.PNG)


## Setup
What are the project requirements/dependencies? Where are they listed? A requirements.txt or a Pipfile.lock file perhaps? Where is it located?

Proceed to describe how to install / setup one's local environment / get started with the project.


## Usage
How does one go about using it?
Provide various use cases and code examples here.

`write-your-code-here`


## Project Status
Project is: In progress. Front and back end microservices are working in a local instance.

## Next Steps 
Jose Tabares- Add the ability for progress tracking on each task card
Fernando Perez- Add new hyperlink and task discription fields onto each new task
Rain Lawson- Debug spring security login and add logout button  
Chris Metzler- Troubleshoot login feature to allow task creation.  
Patrick Stantley- Show up  


## Contributions
Sprint 1:  
Jose Tabares - Changed Front-End Appearance found in https://bitbucket.org/cs3398-f21-ferengi/taskflo/src/development/employeemanagerapp-master/  
Fernando Perez - Changed Back-End file names (Refactoring)  
Rain Lawson- Designed basic GUI  
Chris Metzler- Set up and created local database  

Sprint 2:  
Jose Tabares - Added a button with color input to the add task and edit task models, connected input to javascript variable and added call for changes in the backend https://bitbucket.org/cs3398-f21-ferengi/taskflo/src/master/employeemanagerapp-master/src/app/
The button was added to be able to color code tasks for better organization of tasks. The default color is white and can be changed in the adding of a task or editing.  
Fernando Perez - Updated Task object class to change fields and implement color feature. Also created API calls for the color picker but they were scratched after we developed a completely new color picker. https://bitbucket.org/cs3398-f21-ferengi/taskflo/src/demobranch/employeemanager-master/src/main/java/tech/getarrays/taskmanager/model/Task.java  
Rain Lawson- Created MyUserDetails.java, UserRepository.java, and MyUserDetailsService.java files for the login system found in https://bitbucket.org/cs3398-f21-ferengi/taskflo/src/development/employeemanager-master/src/main/java/tech/getarrays/taskmanager/  
Chris Metzler- Created user class file for backend login, found in https://bitbucket.org/cs3398-f21-ferengi/taskflo/src/development/employeemanager-master/src/main/java/tech/getarrays/taskmanager/model/User.java, and troubleshot backend connectivity to live database and verified its working for everyones local tests.  

Sprint 3:

## Room for Improvement

Room for improvement:
- Improve distribution of work for next sprint
- improve efficiency of code
- improve readability of code

To do:
- Add calendar
- Add feature to see finished tasks


## Acknowledgements
Give credit here.
- This project was inspired by...
- This project was based on [this tutorial](https://www.example.com).
- Many thanks to...


## Contact
Created by [@flynerdpl](https://www.flynerd.pl/) - feel free to contact me!


<!-- Optional -->
<!-- ## License -->
<!-- This project is open source and available under the [... License](). -->

<!-- You don't have to include all sections - just the one's relevant to your project -->