package tech.getarrays.employeemanager.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.getarrays.employeemanager.exception.UserNotFoundException;
import tech.getarrays.employeemanager.model.Employee;
import tech.getarrays.employeemanager.repo.EmployeeRepo;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@Service
@Transactional
public class EmployeeService {
    // Below line injects EmployeeRepo interface
    private final EmployeeRepo employeeRepo;

    @Autowired
    public EmployeeService(EmployeeRepo employeeRepo) {
        this.employeeRepo = employeeRepo;
    }
    // The above line is for constructing

    public Employee addEmployee(Employee employee) {
        // setEmployeeCode is from the Employee class getters/setters
        employee.setEmployeeCode(UUID.randomUUID().toString());
        // save comes from the JpaRepository that's part of the EmployeeRepo interface
        return employeeRepo.save(employee);
    }

    public List<Employee> findAllEmployees() {
        return employeeRepo.findAll();
    }

    public Employee updateEmployee(Employee employee) {
        return employeeRepo.save(employee);
    }

    public Employee findEmployeeById(Long id) {
        return employeeRepo.findEmployeeById(id)
                .orElseThrow(() -> new UserNotFoundException("User by id " + id + " was not found"));
        // The above lines look for an employee id, but if not found throws an exception
        // The UserNotFoundException had to be created in the "exception" folder
    }

    public void deleteEmployee(Long id){
        employeeRepo.deleteEmployeeById(id);
    }
    // The above works because spring understands the language due to it being in what's called "query method"
    // so spring automatically knows what it's trying to do and does it
}
