package tech.getarrays.employeemanager.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import tech.getarrays.employeemanager.model.Employee;

import java.util.Optional;

// JpaRepository is a class that handles saving/loading stuff
public interface EmployeeRepo extends JpaRepository<Employee, Long> {
    void deleteEmployeeById(Long id);

    // Optional makes it so it doesn't break if not passed an id, ie when no employee found
    Optional<Employee> findEmployeeById(Long id);
}
